import typing
import inspect
from contextlib import suppress
from functools import wraps


def coerce(dtcls):
    for field in dataclasses.fields(dtcls):
        value = getattr(dtcls, field.name)
        if not isinstance(value, field.type):
            setattr(dtcls, field.name, field.type(value))